export interface Downloads {
  downloads: number;
  end: string;
  package: string;
  start: string;
}
