export interface Bugs {
  url: string;
}

export interface Repository {
  type: string;
  url: string;
  directory: string;
}

export interface Engines {
  node: string;
}

export interface Dist {
  integrity: string;
  shasum: string;
  tarball: string;
  fileCount: number;
  unpackedSize: number;
  "npm-signature": string;
}

export interface NpmUser {
  name: string;
  email: string;
}

export interface Directories {}

export interface Maintainer {
  name: string;
  email: string;
}

export interface NpmOperationalInternal {
  host: string;
  tmp: string;
}

export interface Data {
  name: string;
  description: string;
  keywords: string[];
  version: string;
  homepage: string;
  bugs: Bugs;
  license: string;
  main: string;
  repository: Repository;
  engines: Engines;
  dependencies: any;
  _id: string;
  _nodeVersion: string;
  _npmVersion: string;
  dist: Dist;
  _npmUser: NpmUser;
  directories: Directories;
  maintainers: Maintainer[];
  _npmOperationalInternal: NpmOperationalInternal;
  _hasShrinkwrap: boolean;
}
