export interface Config {
  options: string[];
  packageManager: "npm" | "yarn";
  packageName: string;
  version: string;
}
