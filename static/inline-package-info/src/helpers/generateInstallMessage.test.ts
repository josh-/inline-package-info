import generateInstallMessage from "./generateInstallMessage";

it("handles package without a version", () => {
  expect(generateInstallMessage("test", null, false, "npm")).toEqual(
    "npm install test"
  );
  expect(generateInstallMessage("test", null, false, "yarn")).toEqual(
    "yarn add test"
  );
});

it("handles package with a version", () => {
  expect(generateInstallMessage("test", "1.0.0", false, "npm")).toEqual(
    "npm install test@1.0.0"
  );
  expect(generateInstallMessage("test", "1.0.0", false, "yarn")).toEqual(
    "yarn add test@1.0.0"
  );
});

it("handles global installs", () => {
  expect(generateInstallMessage("test", null, true, "npm")).toEqual(
    "npm install -g test"
  );
  expect(generateInstallMessage("test", null, true, "yarn")).toEqual(
    "yarn global add test"
  );
});
