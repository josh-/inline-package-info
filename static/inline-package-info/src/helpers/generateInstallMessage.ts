export default (
  packageName: string,
  packageVersion: string | null,
  global: boolean,
  packageManager: "npm" | "yarn"
) => {
  let binary = "";

  switch (packageManager) {
    case "npm":
      binary = "npm";
      break;
    case "yarn":
      binary = "yarn";
      break;
    default:
      throw new Error("Unsupported package manager");
  }

  let options = "";

  if (global) {
    switch (packageManager) {
      case "npm":
        options = "install -g";
        break;
      case "yarn":
        options = "global add";
        break;
      default:
        throw new Error("Unsupported package manager");
    }
  } else {
    switch (packageManager) {
      case "npm":
        options = "install";
        break;
      case "yarn":
        options = "add";
        break;
      default:
        throw new Error("Unsupported package manager");
    }
  }

  let version = "";
  if (packageVersion) {
    version = `@${packageVersion}`;
  }

  return `${binary} ${options} ${packageName}${version}`;
};
