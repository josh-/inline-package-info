import styled from "styled-components";

const Container = styled.div`
  border-top-left-radius: 1.5px;
  border-top-right-radius: 1.5px;
  border-bottom-right-radius: 1.5px;
  border-bottom-left-radius: 1.5px;
  border-top-width: 2px;
  border-right-width: 2px;
  border-bottom-width: 2px;
  border-left-width: 2px;
  border-top-style: solid;
  border-right-style: solid;
  border-bottom-style: solid;
  border-left-style: solid;
  border-top-color: transparent;
  border-right-color: transparent;
  border-bottom-color: transparent;
  border-left-color: transparent;
  margin: 3px;
  box-shadow: rgb(9 30 66 / 25%) 0px 1px 1px, rgb(9 30 66 / 13%) 0px 0px 1px 1px;
`;

export { Container };
