import styled from "styled-components";

const Description = styled.span`
  display: -webkit-box;
  overflow: hidden;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  overflow-wrap: break-word;
  font-size: 12px;
  text-overflow: ellipsis;
`;

export { Description };
