import { useRef, useState } from "react";
import Textfield from "@atlaskit/textfield";
import Button from "@atlaskit/button/standard-button";
import InlineMessage from "@atlaskit/inline-message";

enum CopyMessageStatus {
  None,
  Success,
  Failure,
}

interface CopyableTextFieldProps {
  value: string | null;
}

const CopyableTextField = ({ value }: CopyableTextFieldProps) => {
  const [copyMessageStatus, setCopyMessageStatus] = useState(
    CopyMessageStatus.None
  );
  const textAreaRef = useRef<HTMLInputElement>(null);

  return (
    <Textfield
      isMonospaced
      isReadOnly
      ref={textAreaRef}
      width="xlarge"
      value={value ?? ""}
      style={{ height: "28px" }}
      elemAfterInput={
        <>
          {copyMessageStatus === CopyMessageStatus.Success && (
            <InlineMessage type="confirmation" />
          )}
          {copyMessageStatus === CopyMessageStatus.Failure && (
            <InlineMessage type="error" />
          )}
          <Button
            spacing="compact"
            onClick={() => {
              if (textAreaRef.current) {
                textAreaRef.current.select();
              }
              setCopyMessageStatus(
                document.execCommand("copy") === true
                  ? CopyMessageStatus.Success
                  : CopyMessageStatus.Failure
              );

              setTimeout(() => {
                setCopyMessageStatus(CopyMessageStatus.None);
              }, 3_000);
            }}
          >
            Copy
          </Button>
        </>
      }
    />
  );
};

export default CopyableTextField;
