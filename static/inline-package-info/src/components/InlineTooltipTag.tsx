import React from "react";

const InlineTooltipTag = React.forwardRef(
  (props, ref: React.ForwardedRef<HTMLElement>) => {
    const { children, ...rest } = props;
    return (
      <div
        {...rest}
        // @ts-ignore
        ref={ref}
        style={{
          display: "inline",
        }}
      >
        {children}
      </div>
    );
  }
);

export { InlineTooltipTag };
