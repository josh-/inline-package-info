import React, { useEffect, useState, useRef } from "react";
import { invoke } from "@forge/bridge";
import SectionMessage from "@atlaskit/section-message";
import Tooltip from "@atlaskit/tooltip";
import Spinner from "@atlaskit/spinner";
import InfoIcon from "@atlaskit/icon/glyph/info";
import EditFilledIcon from "@atlaskit/icon/glyph/edit-filled";
import LinkIcon from "@atlaskit/icon/glyph/link";
import BitbucketReposIcon from "@atlaskit/icon/glyph/bitbucket/repos";
import { router } from "@forge/bridge";
import { Data } from "./definitions/data";
import { Config } from "./definitions/config";
import { Downloads } from "./definitions/downloads";
import { ReactComponent as NPMLogo } from "./npm.svg";
import { InlineTooltipTag } from "./components/InlineTooltipTag";
import generateInstallMessage from "./helpers/generateInstallMessage";
import CopyableTextField from "./components/CopyableTextfield";
import { Container } from "./components/Container";
import { Description } from "./components/Description";

function App() {
  const [config, setConfig] = useState<Config | null>(null);
  const [data, setData] = useState<Data | string | null>(null);
  const [downloads, setDownloads] = useState<Downloads | null>(null);

  useEffect(() => {
    invoke<Config | null>("getConfig").then(setConfig);
    invoke<Data | null>("getData").then(setData);
    invoke<Downloads | null>("getDownloads").then(setDownloads);
  }, []);

  if (typeof data === "string" && data === "No package set") {
    return (
      <SectionMessage
        title="You need to configure this macro"
        appearance="warning"
      >
        <p>
          While editing the page, select the macro, and click on the{" "}
          <EditFilledIcon size="small" label="pencil" /> icon to display
          configuration options.
        </p>
      </SectionMessage>
    );
  }

  if (typeof data === "string" && data.startsWith("version not found")) {
    return (
      <SectionMessage title="Version not found" appearance="error">
        <p>This version of {config?.packageName} was not found</p>
      </SectionMessage>
    );
  }

  if (typeof data === "string" && data === "Not Found") {
    return (
      <SectionMessage title="Package not found" appearance="error">
        <p>{config?.packageName} was not found on the public NPM registry</p>
      </SectionMessage>
    );
  }

  if (typeof data === "string" && data === "error") {
    return (
      <SectionMessage title="Unexpected error" appearance="error">
        <p>An unexpected error occurred</p>
      </SectionMessage>
    );
  }

  let installMessage = null;
  if (config && data && typeof data != "string") {
    installMessage = generateInstallMessage(
      data.name,
      config.version,
      config.options.includes("global"),
      config.packageManager
    );
  }

  const repoUrl = (repository: { type: string; url: string }) => {
    if (repository.type === "git" && repository.url.startsWith("git+http")) {
      return repository.url.replace("git+http", "http").replace(".git", "");
    }
    return repository.url;
  };

  return (
    <Container>
      {data && typeof data != "string" ? (
        <div style={{ display: "flex", padding: "4px" }}>
          <div
            style={{
              flexBasis: "540px",
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
            }}
          >
            <div>
              <div>
                <NPMLogo />
                <a
                  onClick={() =>
                    router.open(`https://www.npmjs.com/package/${data.name}`)
                  }
                  style={{ cursor: "pointer" }}
                >
                  <span style={{ marginLeft: "5px" }}>{data.name}</span>
                </a>

                <span style={{ marginLeft: "6px", color: "grey" }}>
                  {data.version}
                </span>
                <a
                  style={{ marginLeft: "6px", fill: "#777", cursor: "pointer" }}
                  onClick={() => router.open(data.homepage)}
                >
                  <LinkIcon size="small" label="homepage" />
                </a>
                <a
                  style={{ marginLeft: "6px", fill: "#777", cursor: "pointer" }}
                  onClick={() => router.open(repoUrl(data.repository))}
                >
                  <BitbucketReposIcon size="small" label="repository" />
                </a>
              </div>

              <div>
                <Description>{data.description}</Description>
              </div>
            </div>

            <div style={{ marginBottom: "4px" }}>
              <CopyableTextField value={installMessage} />
            </div>
          </div>
          <div style={{ display: "flex", flex: 1 }}>
            <div
              style={{
                paddingRight: "8px",
                textAlign: "right",
                flex: 1,
                justifyContent: "space-between",
                display: "flex",
                flexDirection: "column",
              }}
            >
              <div>
                <h6>License</h6>
                <p style={{ margin: "2px 0px 4px" }}>
                  {data.license ?? "Unknown"}
                </p>
              </div>
              <div>
                <h6>Downloads</h6>
                <p style={{ margin: "2px 0px 4px" }}>
                  {downloads?.downloads
                    ? new Intl.NumberFormat().format(downloads.downloads)
                    : ""}

                  <Tooltip
                    content="Downloads of all versions in the last week"
                    tag={InlineTooltipTag}
                  >
                    <InfoIcon size="small" label="info" />
                  </Tooltip>
                </p>
              </div>
            </div>
            <div
              style={{
                textAlign: "right",
                flex: 1,
                justifyContent: "space-between",
                display: "flex",
                flexDirection: "column",
              }}
            >
              <div>
                <h6>Size</h6>
                <p style={{ margin: "2px 0px 4px" }}>
                  {data?.dist?.unpackedSize
                    ? new Intl.NumberFormat("en", {
                        style: "unit",
                        unit: "kilobyte",
                      }).format(data?.dist?.unpackedSize / 1000)
                    : "Unknown"}
                </p>
              </div>
              <div>
                <h6>Maintainers</h6>
                <p style={{ margin: "2px 0px 4px" }}>
                  {data?.maintainers?.length}
                </p>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div
          style={{
            display: "flex",
            justifyContent: "space-around",
            alignItems: "center",
          }}
        >
          <Spinner size="large" />
        </div>
      )}
    </Container>
  );
}

export default App;
