import Resolver from "@forge/resolver";
import { fetch } from "@forge/api";
import ForgeUI, {
  MacroConfig,
  TextField,
  CheckboxGroup,
  Checkbox,
  RadioGroup,
  Radio,
  render,
} from "@forge/ui";

const resolver = new Resolver();

const defaultConfig = {};

resolver.define("getConfig", ({ context }) => {
  const { extension } = context;
  const config = extension.config || defaultConfig;

  return config;
});

resolver.define("getData", async ({ context }) => {
  const { extension } = context;
  const config = extension.config || defaultConfig;

  const packageName = config.packageName;

  if (
    packageName === null ||
    packageName === undefined ||
    packageName.trim() === ""
  ) {
    return "No package set";
  }

  const packageVersion = config.version ? config.version : "latest";
  const apiRoute = `https://registry.npmjs.org/${packageName}/${packageVersion}`;

  try {
    const response = await fetch(apiRoute);

    const json = await response.json();

    // Handle weird response from registry when a version isn't found
    if (typeof json === "object" && json.code === "MethodNotAllowedError") {
      return "version not found";
    }
    return json;
  } catch (error) {
    console.error(error);
    return "error";
  }
});

resolver.define("getDownloads", async ({ context }) => {
  const { extension } = context;
  const config = extension.config || defaultConfig;

  const packageName = config.packageName;
  const apiRoute = `https://api.npmjs.org/downloads/point/last-week/${packageName}`;

  try {
    const response = await fetch(apiRoute);
    return await response.json();
  } catch (error) {
    console.error(error);
    return "error";
  }
});

export const handler = resolver.getDefinitions();

const Config = () => {
  return (
    <MacroConfig>
      <TextField name="packageName" label="Package" />
      <TextField
        name="version"
        label="Version"
        description="By default the latest version will be shown"
      />

      <CheckboxGroup label="Installation options" name="options">
        <Checkbox value="global" label="Global" />
      </CheckboxGroup>

      <RadioGroup name="packageManager" label="Default package manager">
        <Radio defaultChecked label="NPM" value="npm" />
        <Radio label="Yarn" value="yarn" />
      </RadioGroup>
    </MacroConfig>
  );
};

export const config = render(<Config />);
